/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.matjeni.data;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Lawrence Hans Matjeni <lmatjeni@gmail.com at Pabiki Studio>
 */
@Entity
@Table(name = "tax", catalog = "tax_db", schema = "")
@NamedQueries({
    @NamedQuery(name = "Tax.findAll", query = "SELECT t FROM Tax t"),
    @NamedQuery(name = "Tax.findByTaxID", query = "SELECT t FROM Tax t WHERE t.taxID = :taxID"),
    @NamedQuery(name = "Tax.findByTaxIncome", query = "SELECT t FROM Tax t WHERE t.taxIncome = :taxIncome"),
    @NamedQuery(name = "Tax.findByTaxRebate", query = "SELECT t FROM Tax t WHERE t.taxRebate = :taxRebate"),
    @NamedQuery(name = "Tax.findByAge", query = "SELECT t FROM Tax t WHERE t.age = :age"),
    @NamedQuery(name = "Tax.findByMedAidDependant", query = "SELECT t FROM Tax t WHERE t.medAidDependant = :medAidDependant"),
    @NamedQuery(name = "Tax.findByYear", query = "SELECT t FROM Tax t WHERE t.year = :year"),
    @NamedQuery(name = "Tax.findByStatus", query = "SELECT t FROM Tax t WHERE t.status = :status"),
    @NamedQuery(name = "Tax.findByGrossSalary", query = "SELECT t FROM Tax t WHERE t.grossSalary = :grossSalary"),
    @NamedQuery(name = "Tax.findByNettSalary", query = "SELECT t FROM Tax t WHERE t.nettSalary = :nettSalary"),
    @NamedQuery(name = "Tax.findByPayeb4", query = "SELECT t FROM Tax t WHERE t.payeb4 = :payeb4"),
    @NamedQuery(name = "Tax.findByPayeb5", query = "SELECT t FROM Tax t WHERE t.payeb5 = :payeb5"),
    @NamedQuery(name = "Tax.findByEmailAddress", query = "SELECT t FROM Tax t WHERE t.emailAddress = :emailAddress")})
public class Tax implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "taxID")
    private Integer taxID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxIncome")
    private double taxIncome;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxRebate")
    private double taxRebate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "age")
    private int age;
    @Basic(optional = false)
    @NotNull
    @Column(name = "medAidDependant")
    private int medAidDependant;
    @Basic(optional = false)
    @NotNull
    @Column(name = "year")
    private short year;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "status")
    private String status;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gross_salary")
    private double grossSalary;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nett_salary")
    private double nettSalary;
    @Basic(optional = false)
    @NotNull
    @Column(name = "payeb4")
    private double payeb4;
    @Basic(optional = false)
    @NotNull
    @Column(name = "payeb5")
    private double payeb5;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "email_address")
    private String emailAddress;

    public Tax() {
    }

    public Tax(Integer taxID) {
        this.taxID = taxID;
    }

    public Tax(Integer taxID, double taxIncome, double taxRebate, int age, int medAidDependant, short year, String status, double grossSalary, double nettSalary, double payeb4, double payeb5, String emailAddress) {
        this.taxID = taxID;
        this.taxIncome = taxIncome;
        this.taxRebate = taxRebate;
        this.age = age;
        this.medAidDependant = medAidDependant;
        this.year = year;
        this.status = status;
        this.grossSalary = grossSalary;
        this.nettSalary = nettSalary;
        this.payeb4 = payeb4;
        this.payeb5 = payeb5;
        this.emailAddress = emailAddress;
    }

    public Integer getTaxID() {
        return taxID;
    }

    public void setTaxID(Integer taxID) {
        this.taxID = taxID;
    }

    public double getTaxIncome() {
        return taxIncome;
    }

    public void setTaxIncome(double taxIncome) {
        this.taxIncome = taxIncome;
    }

    public double getTaxRebate() {
        return taxRebate;
    }

    public void setTaxRebate(double taxRebate) {
        this.taxRebate = taxRebate;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getMedAidDependant() {
        return medAidDependant;
    }

    public void setMedAidDependant(int medAidDependant) {
        this.medAidDependant = medAidDependant;
    }

    public short getYear() {
        return year;
    }

    public void setYear(short year) {
        this.year = year;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getGrossSalary() {
        return grossSalary;
    }

    public void setGrossSalary(double grossSalary) {
        this.grossSalary = grossSalary;
    }

    public double getNettSalary() {
        return nettSalary;
    }

    public void setNettSalary(double nettSalary) {
        this.nettSalary = nettSalary;
    }

    public double getPayeb4() {
        return payeb4;
    }

    public void setPayeb4(double payeb4) {
        this.payeb4 = payeb4;
    }

    public double getPayeb5() {
        return payeb5;
    }

    public void setPayeb5(double payeb5) {
        this.payeb5 = payeb5;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (taxID != null ? taxID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tax)) {
            return false;
        }
        Tax other = (Tax) object;
        if ((this.taxID == null && other.taxID != null) || (this.taxID != null && !this.taxID.equals(other.taxID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.matjeni.data.Tax[ taxID=" + taxID + " ]";
    }
    
}
