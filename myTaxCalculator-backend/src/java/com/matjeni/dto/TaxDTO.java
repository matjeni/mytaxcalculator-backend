/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.matjeni.dto;

import com.matjeni.data.Tax;
import java.io.Serializable;

/**
 *
 * @author Lawrence Hans Matjeni
 */
public class TaxDTO implements Serializable{
    private double taxIncome,taxRebate;
    private int age,medAidDependant,taxID;
    private short year;
    private String status;

    public TaxDTO() {
    }
    
    public TaxDTO(Tax a) {
        if(a.getTaxID() != null){
            taxID = a.getTaxID();
        }
        taxIncome = a.getTaxIncome();
        taxRebate = a.getTaxRebate();
        age = a.getAge();
        medAidDependant = a.getMedAidDependant();
        year = a.getYear();
        status = a.getStatus();
    }

    public double getTaxIncome() {
        return taxIncome;
    }

    public void setTaxIncome(double taxIncome) {
        this.taxIncome = taxIncome;
    }

    public double getTaxRebate() {
        return taxRebate;
    }

    public void setTaxRebate(double taxRebate) {
        this.taxRebate = taxRebate;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getMedAidDependant() {
        return medAidDependant;
    }

    public void setMedAidDependant(int medAidDependant) {
        this.medAidDependant = medAidDependant;
    }

    public int getTaxID() {
        return taxID;
    }

    public void setTaxID(int taxID) {
        this.taxID = taxID;
    }

    public short getYear() {
        return year;
    }

    public void setYear(short year) {
        this.year = year;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
    
}
