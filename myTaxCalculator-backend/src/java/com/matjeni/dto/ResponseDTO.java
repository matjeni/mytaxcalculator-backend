/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.matjeni.dto;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Lawrence Hans Matjeni
 */
public class ResponseDTO implements Serializable {

    private int statusCode;
    private String message, sessionID;
    private String requestStatus;
    public static final int OK = 0;
    public static final int SERVER_ERROR = 100;
    public static final int DATABASE_ERROR = 101;
    public static final int DATA_NOT_FOUND = 102;
    public static final int UNKNOWN_REQUEST = 103;

    private TaxDTO taxDTO;
    private List<TaxDTO> taxDTOs;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public TaxDTO getTaxDTO() {
        return taxDTO;
    }

    public void setTaxDTO(TaxDTO taxDTO) {
        this.taxDTO = taxDTO;
    }

    public List<TaxDTO> getTaxDTOs() {
        return taxDTOs;
    }

    public void setTaxDTOs(List<TaxDTO> taxDTOs) {
        this.taxDTOs = taxDTOs;
    }

}
