/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.matjeni.dto;

import java.io.Serializable;

/**
 *
 * @author Lawrence Hans Matjeni
 */
public class RequestDTO implements Serializable{
    
    private boolean zippedResponse;
    public static final int GET_TAX = 1;
    private int TaxID;
    private TaxDTO mTaxDTO;
    private String status, sessionID;
    private int requestType;

    public boolean isZippedResponse() {
        return zippedResponse;
    }

    public void setZippedResponse(boolean zippedResponse) {
        this.zippedResponse = zippedResponse;
    }

    public int getTaxID() {
        return TaxID;
    }

    public void setTaxID(int TaxID) {
        this.TaxID = TaxID;
    }

    public TaxDTO getmTaxDTO() {
        return mTaxDTO;
    }

    public void setmTaxDTO(TaxDTO mTaxDTO) {
        this.mTaxDTO = mTaxDTO;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public int getRequestType() {
        return requestType;
    }

    public void setRequestType(int requestType) {
        this.requestType = requestType;
    }
    
    
}
