/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.matjeni.servlet;

import com.google.gson.Gson;
import com.matjeni.dto.RequestDTO;
import com.matjeni.dto.ResponseDTO;
import com.matjeni.util.DataUtil;
import com.matjeni.util.TaxUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sdumane
 */
@WebServlet(name = "TaxMain", urlPatterns = {"/taxMain"})
public class TaxCalculatorMain extends HttpServlet {

    private static final Logger logger = Logger.getLogger(TaxCalculatorMain.class.getName());

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        long start = System.currentTimeMillis();
        Gson gson = new Gson();
        ResponseDTO resp = new ResponseDTO();
        RequestDTO dto = getRequest(request);
        if (dto != null) {
            logger.log(Level.INFO, "###### ---> Starting TAX main: requestType = {0}", dto.getRequestType());
        }
        if (dto == null) {
            response.setContentType("application/json;charset=UTF-8");
            PrintWriter out = response.getWriter();
            resp.setStatusCode(ResponseDTO.UNKNOWN_REQUEST);
            resp.setMessage("Bad request, ignored");
            out.println(gson.toJson(resp));
            out.close();
            return;
        }
        try {
            resp.setStatusCode(ResponseDTO.OK);
            switch(dto.getRequestType()){
            
                case RequestDTO.GET_TAX:
                   
            }
            
        } catch (Exception e) {
        }
    }

    private RequestDTO getRequest(HttpServletRequest req) {
        Gson g = new Gson();
        RequestDTO dto = null;
        try {
            String j = req.getParameter("JSON");
            logger.log(Level.INFO, "### received JSON = {0}", j);
            dto = g.fromJson(req.getParameter("JSON"), RequestDTO.class);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "JSON input error", e);
        }
        return dto;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
