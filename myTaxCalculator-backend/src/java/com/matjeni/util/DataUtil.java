/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.matjeni.util;

import com.matjeni.data.Tax;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Lawrence Hans Matjeni
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class DataUtil {

    private static final Logger log = Logger.getLogger("DataUtil");

    @PersistenceContext
    private static EntityManager em;

    public Tax TAXbyAge(int age) {
        return em.find(Tax.class, age);
    }
}
