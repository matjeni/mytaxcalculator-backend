/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.matjeni.util;

import com.matjeni.data.Tax;
import com.matjeni.dto.ResponseDTO;
import com.matjeni.dto.TaxDTO;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
/**
 *
 * @author Sdumane
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class TaxUtil {
    private static final Logger log = Logger.getLogger("TaxUtil");
    
    @PersistenceContext(unitName = "myTaxCalculator-backendPU")
    private EntityManager em;
    
    @EJB
    DataUtil DataUtil;
    
    public ResponseDTO getTaxByAge(int age){
    
        ResponseDTO resp = new ResponseDTO();
        
        
        try {
            Query q = em.createQuery("select a from Tax a where a.age = :age");
            q.setParameter("age", 45);
            List<Tax>  list = q.getResultList();
            List<TaxDTO> taxDTO = new VirtualFlow.ArrayLinkedList<>();
            if(!list.isEmpty()){
                for (Tax t : list) {
                    taxDTO.add(new TaxDTO(t));
                }
                resp.setStatusCode(0);
                resp.setMessage("Calculation returnd");
                resp.setTaxDTOs(taxDTO);
                log.log(Level.INFO, "***** Calculations Found: {0}",taxDTO.size());
            }else
            {
            resp.setStatusCode(DataException.DATA_NOT_FOUND);
            resp.setMessage("Dololo Records");
            log.log(Level.INFO, "***** Calculations Found: {0}",taxDTO.size());
            }
           
        } catch (Exception e) {
            log.log(Level.SEVERE, "TAXByAge Error",e);
            resp.setStatusCode(DataException.UNKNOWN_ERROR);
        }
        return resp;
    }
}
